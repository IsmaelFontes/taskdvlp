let tareas = [];

let submit, titulo, descripcion, dias, dificultad, prioridad;
var myModal = new bootstrap.Modal(document.getElementById("exampleModal"), {
  keyboard: false,
});
titulo = document.getElementById("titulo");
submit = document.getElementById("boton");
descripcion = document.getElementById("descripcion");
dias = document.getElementById("cantDias");
dificultad = document.getElementById("dif");
prioridad = document.getElementById("prio");
//Evento click me disparo y desde aca ejecuto una funcion
submit.addEventListener("click", miFuncion);
const listitas = document.querySelector(".container ul");
//Funcion crear tarea, que me agrega la tarea al array tareas
function miFuncion(e) {
  e.preventDefault();
  let nuevaTarea = {
    descripcion: descripcion.value,
    dias: dias.value,
    titulo: titulo.value,
    dificultad: dificultad.value,
    prioridad: prioridad.value,
  };

  tareas.push(nuevaTarea);
  tareas.forEach((element) => console.log(element));

  tareas = [];

  const elem = document.createElement("li");
  elem.innerHTML = `<span class='text'> 
    ${nuevaTarea.titulo} 
    ${nuevaTarea.dificultad}
    ${nuevaTarea.prioridad}
    ${nuevaTarea.dias}
    ${nuevaTarea.descripcion}      </span>`;
  elem.draggable = true;
  listitas.append(elem);

  titulo.value = "";
  descripcion.value = "";
  myModal.hide();
  dificultad.value = 1;
  prioridad.value = "Media";
}

// ------ Particles ------- no borrar

particlesJS("particles-js", {
  particles: {
    number: {
      value: 355,
      density: {
        enable: true,
        value_area: 789.1476416322727,
      },
    },
    color: {
      value: "#ffffff",
    },
    shape: {
      type: "circle",
      stroke: {
        width: 0,
        color: "#000000",
      },
      polygon: {
        nb_sides: 5,
      },
      image: {
        src: "img/github.svg",
        width: 100,
        height: 100,
      },
    },
    opacity: {
      value: 0.48927153781200905,
      random: false,
      anim: {
        enable: true,
        speed: 0.2,
        opacity_min: 0,
        sync: false,
      },
    },
    size: {
      value: 2,
      random: true,
      anim: {
        enable: true,
        speed: 2,
        size_min: 0,
        sync: false,
      },
    },
    line_linked: {
      enable: false,
      distance: 150,
      color: "#ffffff",
      opacity: 0.4,
      width: 1,
    },
    move: {
      enable: true,
      speed: 0.2,
      direction: "none",
      random: true,
      straight: false,
      out_mode: "out",
      bounce: false,
      attract: {
        enable: false,
        rotateX: 600,
        rotateY: 1200,
      },
    },
  },
  interactivity: {
    detect_on: "canvas",
    events: {
      onhover: {
        enable: true,
        mode: "bubble",
      },
      onclick: {
        enable: true,
        mode: "push",
      },
      resize: true,
    },
    modes: {
      grab: {
        distance: 400,
        line_linked: {
          opacity: 1,
        },
      },
      bubble: {
        distance: 83.91608391608392,
        size: 1,
        duration: 3,
        opacity: 1,
        speed: 3,
      },
      repulse: {
        distance: 200,
        duration: 0.4,
      },
      push: {
        particles_nb: 4,
      },
      remove: {
        particles_nb: 2,
      },
    },
  },
  retina_detect: true,
});
